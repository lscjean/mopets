window.dataLayer = window.dataLayer || [];
window.dataLayer.push({
    "event" : "Interaction",
    "name" : 
        "like" || "comment" || "share" || "add_friend" || 
        "publish_status" || "publish_photo" || "enter_chat",
    "details": {
        "ts" : "2019-04-01T08:00:00",
    },
    "vpv": {
        "virtualPageName" : "",
        "virtualPageURL" : "",
    }
    
});