window.dataLayer = window.dataLayer || [];
window.dataLayer.push({
    "event" : "Interaction",
    "action" : "publish_photo" ,
    "details": {
        "timestamp" : "2019-04-01T08:00:00",
    },
    "vpv": {
        "virtualPageName" : "", // insert page title
        "virtualPageURL" : "", // insert page url
    }   
});