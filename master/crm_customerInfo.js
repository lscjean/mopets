var family_data = {
"logged_in" : true,
"user" : {
    "id" : "123456879",
    "first_name": "john",
    "last_name" : "doe",
    "gender" : "male",
    "dob" : "2019-04-01",
    "age" : "35",
    "email" : "john.doe@me.com",
    "optin" : {
        "timestamp" : "2019-04-01T08:00:00",
        "marketing" : "true",
        "preferences" : "true",
        "statistics" : "true",
    },
    "country" : "belgium",
    "zipcode" : "1000",
    "language" : "dutch",
    "membership" : "2019-04-01T08:00:00",
    "visitor_attribute" : {
        "login_count" : 1,
        "session_count" : 3, // pushed from GTM
        "page_count" : 5, // pushed from GTM
        "interaction_count" : 10, // pushed from GTM
        "friends_count" : 12,
        "isPetCarer" : true,
        "isPetSitter" : true,
        "isPetWalker" : true,
        "gclid" : "123456789.987654321", // pushed from GTM
        "fbp" : "123456789.987654321", // pushed from GTM
        "dclid": "123456789.987654321", // pushed from GTM
        "tdid": "0fb3622d-67b5-4a3f-bd6c-f8d5f85fc656" // pushed from GTM
    },
    "pets" : [
        {
            "id" : "123456789",
            "name" : "doggo",
            "gender" : "female",
            "category" : "dog",
            "breed" : "labrador",
            "age" : "2",
            "color" : "brown",  
        },
        {
            "id" : "987654321",
            "name" : "cate",
            "gender" : "male",
            "category" : "cat",
            "breed" : "siamese",
            "age" : "5",
            "color" : "white",  
        },
    ],
    "visitor_audiences" : ["audience1", "audience2", "audience3", "audienceN"],
},
}
