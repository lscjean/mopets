window.dataLayer = window.dataLayer || [];
window.dataLayer.push({
"event" : "New Pet",
    "pets" : {
        "all_pets" : [
            {
                "id" : "123456789", // family.N.pet.id
                "gender" : "female",
                "category" : "dog",
                "breed" : "labrador",
                "age" : "2",
                "color" : "brown",  
            },
            {
                "id" : "987654321",
                "gender" : "male",
                "category" : "cat",
                "breed" : "siamese",
                "age" : "5",
                "color" : "white",  
            },
        ],
    },
})