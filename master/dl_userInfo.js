window.dataLayer = window.dataLayer || [];
window.dataLayer.push({
"event" : "Family Data",
    "logged_in" : true,
    "user" : {
        "id" : "123456879", // family.id
        "gender" : "male", // to add
        "dob" : "2019-04-01T08:00:00", // to add
        "country" : null, // to add 
        "zipcode" : null,
        "city" : "brussels", // to add
        "language" : "dutch", // family.language
        "membership" : "2019-04-01T08:00:00", // to add
        "profite_attribute" : {
            "isPetCarer" : null, // to add
            "isPetSitter" : null, // to add 
            "isPetWalker" : null, // to add
        },
    },
    "pets" : {
        "active_pet": {
            /*"ts" : '2019-04-01T08:00:00',*/
            "details": {
                "id" : "123456789", // family.0.pet.id
                "gender" : "female", // family.0.pet.gender
                "category" : "dog", // family.0.pet.type.tag
                "breed" : "labrador", // family.0.pet.breed
                "dob" : "2", // family.0.pet.birthday
                /* "color" : "brown", // family.pet. */
            }
        },
        "all_pets" : [
            {
                "id" : "123456789", // family.N.pet.id
                "gender" : "female",
                "category" : "dog",
                "breed" : "labrador",
                "dob" : "2",
                "color" : "brown",  
            },
            {
                "id" : "987654321",
                "gender" : "male",
                "category" : "cat",
                "breed" : "siamese",
                "dob" : "5",
                "color" : "white",  
            },
        ],
    },

    /*"company" : {
        "shelter" : {
            "id" : "123456789",
            "name" : "shelter_1"
        },
        "professional" : {
            "isPremium" : true,
            "id" : "123456789",
            "name" : "professional"
        }
    }*/
})