window.dataLayer = window.dataLayer || [];
window.dataLayer.push({
    "event" : "Funnel",
    "action" : "add_pet",
    "details": {
        "category" : "pet_owner",
        "step_num" : 1,
        "step_name" : "pet_info",
    },
    "vpv": {
        "virtualPageName" : "Pet Info",
        "virtualPageURL" : "/en/add/pet/info",
    }
    
});