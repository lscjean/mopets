window.dataLayer = window.dataLayer || [];
window.dataLayer.push({
    "event" : "Funnel",
    "action" : "add_pet",
    "details": {
        "category" : "pet_owner",
        "step_num" : 2,
        "step_name" : "pet_characteristics",
    },
    "vpv": {
        "virtualPageName" : "Pet Characteristics",
        "virtualPageURL" : "/en/add/pet/characteristics",
    }
    
});