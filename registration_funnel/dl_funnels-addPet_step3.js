window.dataLayer = window.dataLayer || [];
window.dataLayer.push({
    "event" : "Funnel",
    "action" : "add_pet",
    "details": {
        "category" : "pet_owner",
        "step_num" : 3,
        "step_name" : "pet_registration_success",
    },
    "vpv": {
        "virtualPageName" : "Pet Registration Success",
        "virtualPageURL" : "/en/add/pet/registration-success",
    }
    
});