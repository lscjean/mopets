window.dataLayer = window.dataLayer || [];
window.dataLayer.push({
    "event" : "Funnel",
    "id" : 123456,
    "name" : "registration",
    "details": {
        "category" : "pet_services" || "pet_owner" || "pet_professional_free" || "pet_professional_prem" || "pet_shelter",
        "step_num" : 1,
        "step_name" : "personal_information",
        "data" : {
            "custom_field1" :'tbd',
            "custom_field2" :'tbd',
            "custom_fieldn" :'tbd',
        }
    },
    "vpv": {
        "virtualPageName" : "",
        "virtualPageURL" : "",
    }
    
});