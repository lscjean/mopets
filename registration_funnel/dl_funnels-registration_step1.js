window.dataLayer = window.dataLayer || [];
window.dataLayer.push({
    "event" : "Funnel",
    "action" : "registration",
    "details": {
        "category" : "pet_owner",
        "step_num" : 1,
        "step_name" : "owner_info",
    },
    "vpv": {
        "virtualPageName" : "Registration - Owner Info",
        "virtualPageURL" : "/en/register/owner-info",
    }
    
});