window.dataLayer = window.dataLayer || [];
window.dataLayer.push({
    "event" : "Funnel",
    "action" : "registration",
    "details": {
        "category" : "pet_owner",
        "step_num" : 2,
        "step_name" : "pet_info",
    },
    "vpv": {
        "virtualPageName" : "Registration - Pet Info",
        "virtualPageURL" : "/en/register/pet-info",
    }
    
});