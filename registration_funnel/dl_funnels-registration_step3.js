window.dataLayer = window.dataLayer || [];
window.dataLayer.push({
    "event" : "Funnel",
    "action" : "registration",
    "details": {
        "category" : "pet_owner",
        "step_num" : 3,
        "step_name" : "pet_characteristics",
    },
    "vpv": {
        "virtualPageName" : "Registration - Pet Characteristics",
        "virtualPageURL" : "/en/register/pet-characteristics",
    }
    
});