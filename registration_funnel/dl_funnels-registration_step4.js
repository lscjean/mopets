window.dataLayer = window.dataLayer || [];
window.dataLayer.push({
    "event" : "Funnel",
    "action" : "registration",
    "details": {
        "category" : "pet_owner",
        "step_num" : 4,
        "step_name" : "account_registration_success",
    },
    "vpv": {
        "virtualPageName" : "Registration - Account Registration Success",
        "virtualPageURL" : "/en/register/account-registration-success",
    }
    
});